#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <vector>
#include <cstring>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#include "core/Rectangle.h"
#include "core/Genome.h"
#include "core/GenePool.h"
#include "core/Backtrack.h"

std::vector<sf::Rect<float> > rects;

int runGenetic()
{
	GenePool gpool(rects);
	int genNum = 0;

	// Create the main rendering window
	sf::RenderWindow app(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 32), "Pack");
	app.setFramerateLimit(60);
	while (app.isOpen())
	{
		if (genNum < 100)
		{
			gpool.evolve();
			std::cout << "gen num:" << genNum++ << std::endl;
		}
		
		// process events
		sf::Event event;
		while (app.pollEvent(event))
		{
			// close window : exit
			if (event.type == sf::Event::Closed)
			{
				app.close();
			}
		}
		// clear screen
		app.clear();
		Triangle tri = gpool.getBest().getContainingTriangle();
		float dd = -tri.getVertexVector(0).x+tri.getVertexVector(1).x;
		sf::Vector2f size(dd, dd*M_SQRT3/2.0);
		sf::Vector2f pos;
		pos = tri.getVertexVector(0) + tri.getVertexVector(1);
		pos /= 4.0f;
		pos+=tri.getVertexVector(2)/2.0f;
		
		sf::View view(pos, size);
		app.setView(view);
		
		// do drawing
		{
			// draw containing triangle
			/*
			Triangle ct = gpool.getWorst().getContainingTriangle();
			sf::Vertex verts[3];
			verts[0] = ct.getVertex(0);
			verts[1] = ct.getVertex(1);
			verts[2] = ct.getVertex(2);
			app.draw(verts, 3, sf::Triangles);
			*/
			// draw containing triangle
			sf::Vertex verts[3];
			Triangle ct = gpool.getBest().getContainingTriangle();
			verts[0] = ct.getVertex(0);
			verts[1] = ct.getVertex(1);
			verts[2] = ct.getVertex(2);
			app.draw(verts, 3, sf::Triangles);
			
			// draw rectangles
			RenderData rd = gpool.getBest().getRenderData();
			app.draw(rd.vertices, rd.numVertices, sf::Quads);
		}
		
		// display window contents on screen
		app.display();
	}

	return EXIT_SUCCESS;
}

bool compareRect(const sf::Rect<float> &a, const sf::Rect<float> &b)
{
	return a.width * a.height < b.width * b.height;
}

static void reverse(unsigned int *ar, size_t len)
{
    unsigned int i, j;

    for (i = 0, j = len - 1; i < j; i++, j--) {
        std::swap(ar[i], ar[j]);
    }
}

unsigned int permute(unsigned int *ar, size_t len)
{
    unsigned int i1, i2;
    unsigned int result = 0;
    
    for (i1 = len - 2, i2 = len - 1; ar[i2] <= ar[i1] && i1 != 0; i1--, i2--);
    if (ar[i2] <= ar[i1]) {
        reverse(ar, len);
    }
    else {
        for (i2 = len - 1; i2 > i1 && ar[i2] <= ar[i1]; i2--);
        std::swap(ar[i1], ar[i2]);
        reverse(ar + i1 + 1, len - i1 - 1);
        result = 1;
    }
    return result;
}

int runBackrack()
{
	Backtrack bt;
	
	unsigned int *numbers = new unsigned int[rects.size()];
	bool fail;
	
begin:
	for (int i = 0; i<rects.size(); i++)
		numbers[i] = i;
    const unsigned int n = rects.size();
	
	fail = true;
	
    do {
		int numFail;
		unsigned int nextNum;
		for (int i = 0; i<n; i++)
		{
			std::cout << numbers[i] << " " ;
		}
		std::cout << std::endl;
		if (( numFail = bt.run(rects, numbers) ) == -1)
		{
			fail = false;
			break;
		} else
		{
			nextNum = numbers[numFail-1];
			std::cout << "failed at:" << nextNum << std::endl;
			// backtrack
			while(numbers[numFail-1] == nextNum)
			{
				permute(numbers, n);
			}
		}
		bt.reset();

    } while (permute(numbers, n));
	if (fail)
	{
		bt.turnIndices[0]++;
		goto begin;
	}
	// Create the main rendering window
	sf::RenderWindow app(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 32), "Pack");
	app.setFramerateLimit(10);
	while (app.isOpen())
	{
		// process events
		sf::Event event;
		while (app.pollEvent(event))
		{
			// close window : exit
			if (event.type == sf::Event::Closed)
			{
				app.close();
			}
		}
		
		// clear screen
		app.clear();
		Triangle tri = bt.triangle;
		float dd = -tri.getVertexVector(0).x+tri.getVertexVector(1).x;
		sf::Vector2f size(dd, dd*M_SQRT3/2.0);
		sf::Vector2f pos;
		pos = tri.getVertexVector(0) + tri.getVertexVector(1);
		pos /= 4.0f;
		pos+=tri.getVertexVector(2)/2.0f;
		sf::View view(pos, size);
		app.setView(view);
		
		// do drawing
		{
			// draw containing triangle
			Triangle ct = bt.triangle;
			sf::Vertex verts[3];
			verts[0] = ct.getVertex(0);
			verts[1] = ct.getVertex(1);
			verts[2] = ct.getVertex(2);
			app.draw(verts, 3, sf::Triangles);
			
			// draw rectangles
			RenderData rd = bt.getRenderData();
			app.draw(rd.vertices, rd.numVertices, sf::Quads);
		}
		
		// display window contents on screen
		app.display();
	}
	
	return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		std::cout << "provide --genetic n or --backtrack n" << std::endl;
		return EXIT_FAILURE;
	}
	srand(time(NULL));
	
	int w, h, i;
	std::cin >> i;
	
	while (i--)
	{
		std::cin >> w >> h;
		rects.push_back(sf::Rect<float>(0, 0, w, h));
	}
	
	if (strcmp(argv[1], "--genetic") == 0)
	{
		return runGenetic();
	} else if (strcmp(argv[1], "--backtrack") == 0)
	{
		std::sort(rects.begin(), rects.end(), compareRect);
		return runBackrack();
	} else
	{
		std::cout << "provide --genetic n or --backtrack n" << std::endl;
	}
	return 0;
}