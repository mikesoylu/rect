/* 
 * File:   Genome.h
 * Author: mikesoylu
 *
 * Created on 08 Ocak 2013 Salı, 20:36
 */

#ifndef GENOME_H
#define	GENOME_H

#define M_SQRT3 1.73205080757
#define ABS(x)(((x)>0)?(x):(-(x)))

#include "Strand.h"
#include "Rectangle.h"
#include "Triangle.h"

struct RenderData
{
	sf::Vertex *vertices;
	int numVertices;
	
	RenderData(int numVertices) : numVertices(numVertices)
	{
		vertices = new sf::Vertex[numVertices];
	}
	~RenderData()
	{
		delete [] vertices;
	}
};

class Genome
{
protected:
	std::vector<Strand> strands;
	float fitness;
public:
	Genome()
	{ }

	Genome(const Genome &cpy)
	{
		std::vector<Strand>::const_iterator i;
		for(i = cpy.strands.begin(); i != cpy.strands.end(); i++)
		{
			Strand s(*i);
			strands.push_back(s);
		}
		fitness = cpy.fitness;
	}
	
	Genome(std::vector<sf::Rect<float> > &rects)
	{
		std::vector<sf::Rect<float> >::iterator i;
		
		for(i = rects.begin(); i != rects.end(); i++)
		{
			Strand s(*i);
			strands.push_back(s);
		}
		fitness = getFitness();
	}
	
	float getFitness() const
	{
		Triangle tri = getContainingTriangle();
		float l = tri.getVertexVector(1).x - tri.getVertexVector(0).x;
		float h = l*M_SQRT3/2;

		int num_overlaps = 0;
		const int num_rays = 40;
		// rays in rects
		float overlaps = 0;
		// rays in spaces
		for (int i = 0; i<strands.size(); i++)
			for (int j = 0; j<strands.size(); j++)
			{
				if (i != j)
				{
					Rectangle r = strands[j].getRect();
					num_overlaps += (int)strands[i].getRect().intersects(r);
				}
			}
		float in_triangle = 0;
		for (int j = 0; j<num_rays; j++)
		{
			sf::Vector2f p((float)rand()/RAND_MAX*l + tri.getVertexVector(0).x,
						  -(float)rand()/RAND_MAX*h + tri.getVertexVector(0).y);
			
			if (tri.intersects(p))
			{
				bool flag = true;
				for(int i = 0; i<strands.size(); i++)
				{
					const Strand &sp = strands[i];
					if (sp.getRect().intersects(p))
					{
						overlaps++;
						flag = false;
					}
				}
				if (flag)
					in_triangle++;
			}
		}
		in_triangle /=num_rays;
		overlaps /= num_rays;
		
		return -(num_overlaps*100 + overlaps + 3*in_triangle)*l*l*M_SQRT3/4.0;
	}
	
	bool operator<(const Genome &rhs) const
	{
		return fitness > rhs.fitness;
	}
	
	void crossover(const Genome &gen)
	{
		int crossover = rand() % gen.strands.size();

		for(int i = 0; i<strands.size(); i++)
		{
			if (i == crossover)
			{
				strands[i].crossover(gen.strands[i]);
				break;
			} else
			{
				strands[i] = gen.strands[i];
			}
		}
		fitness = getFitness();
	}
	
	void print()
	{
		for(int i = 0; i<strands.size(); i++)
		{
			strands[i].print();
		}
	}
	
	void mutate()
	{
		for(int i = 0; i<strands.size(); i++)
		{
			Strand &sp = strands[i];
			sp.mutate(61, 3);
		}
		fitness = getFitness();
	}
	
	Triangle getContainingTriangle() const
	{
		Triangle ret;
		
		sf::Vector2f a(-100000.0f, -100000.0f);
		sf::Vector2f b( 100000.0f, -100000.0f);
		sf::Vector2f c(0.0f, 100000.0f);
		float minax, minay;
		float minbx, minby;
		float mincx, mincy;
		float mina, minb, minc;
		mina = minb = minc = std::numeric_limits<float>::max();

		std::vector<Strand>::const_iterator i;
		for(i = strands.begin(); i!=strands.end(); i++)
		{
			const Strand &sp = *i;
			for (int j=0; j < 4; j++)
			{
				sf::Vector2f vp = sp.getRect().getVertexVector(j);
				float da = (-(a.y-vp.y) - (a.x-vp.x)*M_SQRT3)/2;
				float db = (-(b.y-vp.y) + (b.x-vp.x)*M_SQRT3)/2;
				float dc = (c.y-vp.y);

				if (ABS(da)<mina)
				{
					minax = vp.x;
					minay = vp.y;
					mina = da;
				}
				if (ABS(db)<minb)
				{
					minbx = vp.x;
					minby = vp.y;
					minb = db;
				}
				if (ABS(dc)<minc)
				{
					mincx = vp.x;
					mincy = vp.y;
					minc = dc;
				}
			}
		}
		
		float ha = mincy-minay;
		float hb = mincy-minby;
		float x1 = minax-ha/M_SQRT3;
		float x2 = minbx+hb/M_SQRT3;
		ret.setPoint(0, sf::Vector2f(x1, mincy));
		ret.setPoint(1, sf::Vector2f(x2, mincy));
		ret.setPoint(2, sf::Vector2f((x2+x1)/2,mincy-M_SQRT3*(x2-x1)/2));
		
		return ret;
	}
	
	RenderData getRenderData()
	{
		RenderData rd(strands.size() * 4);
		sf::Vertex *vertp = rd.vertices;
		
		std::vector<Strand>::iterator i;
		for(i = strands.begin(); i!=strands.end(); i++)
		{
			for (int j=0; j < 4; j++)
			{
				(*vertp++) = i->getRect().getVertex(j);
			}
		}
		return rd;
	}
};

#endif	/* GENOME_H */