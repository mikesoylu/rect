/* 
 * File:   GenePool.h
 * Author: mikesoylu
 *
 * Created on 09 Ocak 2013 Çarşamba, 03:20
 */

#ifndef GENEPOOL_H
#define	GENEPOOL_H

#include "Genome.h"

#define MAX_GENERATIONS 1000
#define NUM_CROSSOVERS 500


class GenePool
{
	std::vector<Genome> pool;
	std::vector<Genome> newGen;

public:
	GenePool(std::vector<sf::Rect<float> > &rects)
	{
		for (int i = 0; i<MAX_GENERATIONS; i++)
			pool.push_back(Genome(rects));
	}
	
	void evolve()
	{
		for (int i = 0; i<NUM_CROSSOVERS; i++)
		{
			Genome newg1(getRandom());
			newg1.crossover(getRandom());
			newGen.push_back(newg1);

			if (rand()%100 < 10)
			{
				Genome newg2(getRandom());
				newg2.mutate();
				newGen.push_back(newg2);
			}
		}
		
		endGeneration();
	}
	
	void endGeneration()
	{
		for (int i = 0; i < newGen.size(); i++)
		{
			pool.push_back(newGen[i]);
		}
		newGen.clear();
		sort();
		// clean up
		pool.erase(pool.begin()+MAX_GENERATIONS, pool.end());
	}
	
	Genome &getRandom()
	{
		return pool[rand()%pool.size()];
	}
	
	Genome &getBest()
	{
		return pool.front();
	}
	
	Genome &getWorst()
	{
		return pool.back();
	}
	
	Genome &getSecondBest()
	{
		return *(pool.begin()+1);
	}
	
	void sort()
	{
		std::sort(pool.begin(), pool.end());
	}
};

#endif	/* GENEPOOL_H */

