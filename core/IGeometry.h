/* 
 * File:   IGeometry.h
 * Author: mikesoylu
 *
 * Created on 09 Ocak 2013 Çarşamba, 02:31
 */

#ifndef IGEOMETRY_H
#define	IGEOMETRY_H

class IGeometry
{
public:
	virtual sf::Vector2f getVertexVector(int id) const = 0;
	
	virtual sf::Vertex getVertex(int id) const = 0;
	
	virtual bool intersects(sf::Vector2f p) const = 0;
};

#endif	/* IGEOMETRY_H */