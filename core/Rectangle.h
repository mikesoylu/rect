/* 
 * File:   Rectangle.h
 * Author: mikesoylu
 *
 * Created on 08 Ocak 2013 Salı, 20:52
 */

#ifndef RECTANGLE_H
#define	RECTANGLE_H

#include "IGeometry.h"
#include <limits>
#include <cmath>

/**
 * A rotated rectangle
 * angle is defined as 0->3 = 0 & 0->1 = -90
 * vertices defined as;
 * (0)----------(3)
 *  |            |
 *  |            |
 * (1)----------(2)
 */
class Rectangle : public IGeometry
{
public:
	/** top left */
	sf::Vector2f position;
	
	/** angle in radians */
	float angle;
	float width, height;
	
	sf::Vector2f verts[4];
	
	Rectangle() { }
	
	Rectangle(float x, float y, float width, float height, float angle) : angle(angle), width(width), height(height)
	{
		position = sf::Vector2f(x, y);
		setVertices();
	}
	
	sf::Vector2f getVertexVector(int id) const
	{
		return verts[id];
	}
	
	void setVertices()
	{
		verts[0] = position;
		
		verts[1].x = position.x + height * cosf(angle-M_PI_2);
		verts[1].y = position.y + height * sinf(angle-M_PI_2);
		
		verts[2] = verts[1];
		verts[2].x += width * cosf(angle);
		verts[2].y += width * sinf(angle);
		
		verts[3].x = position.x + width * cosf(angle);
		verts[3].y = position.y + width * sinf(angle);
	}
	
	sf::Vertex getVertex(int id) const
	{
		sf::Vector2f vec = getVertexVector(id);
		sf::Color color;
		color.r = 170+position.x;
		color.g = 170+position.x;
		color.b = 170+position.x;
		color.a = 100;

		return sf::Vertex(vec, color);
	}
	
	const sf::Vector2f &getRight() const
	{
		int ret;
		float max = std::numeric_limits<float>::min();
		for (int i = 0; i<4; i++)
		{
			if (verts[i].x>max)
			{
				ret = i;
				max = verts[i].x;
			}
		}
		return verts[ret];
	}
	
	const sf::Vector2f &getUp() const
	{
		int ret;
		float max = std::numeric_limits<float>::min();
		for (int i = 0; i<4; i++)
		{
			if (verts[i].y>max)
			{
				ret = i;
				max = verts[i].x;
			}
		}
		return verts[ret];
	}
	
	const sf::Vector2f &getDown() const
	{
		int ret;
		float min = std::numeric_limits<float>::max();
		for (int i = 0; i<4; i++)
		{
			if (verts[i].y < min)
			{
				ret = i;
				min = verts[i].x;
			}
		}
		return verts[ret];
	}
	
	const sf::Vector2f getLeft() const
	{
		int ret;
		float min = std::numeric_limits<float>::max();
		for (int i = 0; i<4; i++)
		{
			if (verts[i].x<min)
			{
				ret = i;
				min = verts[i].x;
			}
		}
		return verts[ret];
	}
	
	/** Checks if rectangle is intersecting with point */
	bool intersects(sf::Vector2f p) const
	{
		for (int i1 = 0; i1 < 4; i1++)
		{
			int i2 = (i1 + 1) % 4;
			sf::Vector2f p1 = getVertexVector(i1);
			sf::Vector2f p2 = getVertexVector(i2);
			
			sf::Vector2f normal = sf::Vector2f(p2.y - p1.y, p1.x - p2.x);
			
			float projected = normal.x * (p2.x-p.x) + normal.y * (p2.y-p.y);
			
			if (projected<0)
			{
				return false;
			}
		}
		return true;
	}
	
	/** Checks if rectangles are intersecting */
	bool intersects(const Rectangle &b) const
	{
		// so we don't copy-paste code
		const Rectangle *rects[2] = {this, &b};
		for (int i = 0; i < 2; i++)
		{
			const Rectangle *polygon = rects[i];
			for (int i1 = 0; i1 < 4; i1++)
			{
				int i2 = (i1 + 1) % 4;
				sf::Vector2f p1 = polygon->getVertexVector(i1);
				sf::Vector2f p2 = polygon->getVertexVector(i2);

				sf::Vector2f normal = sf::Vector2f(p2.y - p1.y, p1.x - p2.x);

				float minA = std::numeric_limits<float>::max();
				float maxA = std::numeric_limits<float>::min();
				for (int j = 0; j < 4; j++)
				{
					sf::Vector2f p = getVertexVector(j);
					float projected = normal.x * p.x + normal.y * p.y;
					if (projected < minA)
						minA = projected;
					if (projected > maxA)
						maxA = projected;
				}

				float minB = std::numeric_limits<float>::max();
				float maxB = std::numeric_limits<float>::min();
				for (int j = 0; j < 4; j++)
				{
					sf::Vector2f p = b.getVertexVector(j);
					float projected = normal.x * p.x + normal.y * p.y;
					if (projected < minB)
						minB = projected;
					if (projected > maxB)
						maxB = projected;
				}

				if (maxA < minB || maxB < minA)
					return false;
			}
		}
		return true;
	}
};

#endif	/* RECTANGLE_H */

