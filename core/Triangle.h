/* 
 * File:   Triangle.h
 * Author: mikesoylu
 *
 * Created on 09 Ocak 2013 Çarşamba, 02:30
 */

#ifndef TRIANGLE_H
#define	TRIANGLE_H
/**
 *      (2)
 *     /  \
 *    /    \
 *   /      \
 * (0)------(1)
 */
class Triangle : public IGeometry
{
	sf::Vector2f vertices[3];
	
public:
	Triangle()
	{
		
	}
	
	void setPoint(int id, sf::Vector2f pos)
	{
		vertices[id] = pos;
	}
	
	virtual sf::Vector2f getVertexVector(int id) const
	{
		return vertices[id];
	}
	
	virtual sf::Vertex getVertex(int id) const
	{
		sf::Vector2f vec = getVertexVector(id);
		sf::Color color;
		color.r = (int)vertices[2].x%100+70;
		color.g = (int)vertices[2].y%100+70;
		color.b = (int)(vertices[2].x+vertices[2].y)%100+70;

		return sf::Vertex(vec, color);
	}
	
	virtual bool intersects(sf::Vector2f p) const
	{
		for (int i1 = 0; i1 < 3; i1++)
		{
			int i2 = (i1 + 1) % 3;
			const sf::Vector2f &p1 = getVertexVector(i1);
			const sf::Vector2f &p2 = getVertexVector(i2);
			
			sf::Vector2f normal = sf::Vector2f(p2.y - p1.y, p1.x - p2.x);
			
			float projected = normal.x * (p2.x-p.x) + normal.y * (p2.y-p.y);
			
			if (projected>0)
			{
				return false;
			}
		}
		return true;
	}
};

#endif	/* TRIANGLE_H */