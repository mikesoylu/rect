/* 
 * File:   Backtrack.h
 * Author: mikesoylu
 *
 * Created on 14 Ocak 2013 Pazartesi, 01:21
 */

#ifndef BACKTRACK_H
#define	BACKTRACK_H

#include "Triangle.h"
#include "Strand.h"
#include "Genome.h"

enum Mode{BOTTOM = 0, LEFT, RIGHT};

class Backtrack: public Genome
{
public:
	Mode mode;
	
	Triangle triangle;
	
	// 0: Bottom-Right 1: Right-Left
	int turnIndices[2];
	
	Backtrack()
	{
		mode = BOTTOM;
		turnIndices[0] = 1;
		turnIndices[1] = 0;
	}
	
	bool placeRect(const sf::Rect<float> rect)
	{
		if (BOTTOM == mode)
		{
			placeRectHelper(rect);

			if (strands.size() == turnIndices[0])
			{
				mode = RIGHT;
				triangle = getContainingTriangle();
				std::cout << "turn" << std::endl;
			}
		} else if (RIGHT == mode)
		{
			if (!placeRectHelper(rect))
			{
				mode = LEFT;
				turnIndices[1] = strands.size();
				placeRect(rect); // try again
			}
		} else if (LEFT == mode)
		{
			if (!placeRectHelper(rect))
			{
				return false;
			}
		}
		return true;
	}
	
	bool placeRectHelper(const sf::Rect<float> rect)
	{
		if (!strands.empty())
		{
			Strand &prev = strands.back();
			if (BOTTOM == mode)
			{
				float a = prev.getRect().getRight().x;
				float b = prev.getRect().getUp().y;
				Strand ns(rect, a, b);
				strands.push_back(ns);
				
			} else if (RIGHT == mode || LEFT == mode)
			{
				sf::Vector2f dd;
				Strand ns;
				
				if (RIGHT == mode)
				{
					dd = triangle.getVertexVector(2) - triangle.getVertexVector(1);
					ns = Strand(rect, triangle.getVertexVector(1).x - rect.width*0.5 - rect.height*2/M_SQRT3,
									  triangle.getVertexVector(1).y - rect.width*0.5*M_SQRT3);
					ns.angle = 2;
				} else
				{
					dd = triangle.getVertexVector(0) - triangle.getVertexVector(2);
					ns = Strand(rect, triangle.getVertexVector(2).x - rect.height*0.5/M_SQRT3,
									  triangle.getVertexVector(2).y + rect.height/2);
					ns.angle = 4;
				}
				
				dd /= (float)sqrt(dd.x*dd.x + dd.y*dd.y);
				
				ns.updateRect();
				float x = ns.x, y = ns.y;
				bool intersects = true;
				
				while(intersects)
				{
					x += dd.x;
					y += dd.y;
					ns.x = x;
					ns.y = y;
					ns.updateRect();
					if (RIGHT == mode)
					{
						if (!triangle.intersects(ns.getRect().getVertexVector(0)))
							return false;
					} else
					{
						if (!triangle.intersects(ns.getRect().getVertexVector(2)))
							return false;
					}
					intersects = false;
					for (int i = 0; i<strands.size(); i++)
					{
						if (strands[i].getRect().intersects(ns.getRect()))
						{
							intersects = true;
							break;
						}
					}
				}
				
				strands.push_back(ns);
			}
		} else
		{
			Strand ns(rect);
			ns.angle = 0;
			ns.updateRect();
			strands.push_back(ns);
		}
		return true;
	}
	
	bool runHelper(sf::Rect<float> r)
	{
		if(placeRect(r))
		{
			return true;
		} else
		{
			float tmp = r.width;
			r.width = r.height;
			r.height = tmp;
			if (placeRect(r))
			{
				return true;
			} else
			{
				// we couldn't place this one so don't recurse on this
				return false;
			}
		}
		return false;
	}
	
	int run(std::vector< sf::Rect<float> > arr, unsigned int *ind)
	{
		for (int i=0; i<arr.size(); i++)
		{
			if (!runHelper(arr[ind[i]]))
			{
				// failed at
				return i;
			}
		}
		// success
		return -1;
	}
	
	void reset()
	{
		strands.clear();
		mode = BOTTOM;
		turnIndices[1] = 0;
	}
};

#endif	/* BACKTRACK_H */

