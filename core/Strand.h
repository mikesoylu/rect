/* 
 * File:   Strand.h
 * Author: mikesoylu
 *
 * Created on 08 Ocak 2013 Salı, 20:36
 */

#ifndef STRAND_H
#define	STRAND_H

#include "Rectangle.h"
#include <cstdlib>
#include <cstdio>
#include <climits>

#define NUM_ANGLES 6
#define MAX_XY 1000

class Strand
{
	Rectangle rect;

public:

	/* start of genetic code */
	
	/** top left */
	int x, y;
	/** orientation of the rectangle */
	char angle;
	
	/* end of genetic code */
	
	int width, height;
	
	Strand() { }
	
	Strand(const Strand &cpy)
	{
		x = cpy.x;
		y = cpy.y;
		angle = cpy.angle;
		width = cpy.width;
		height = cpy.height;
		updateRect();
	}
	
	Strand(const sf::Rect<float> &rect, int x, int y) : x(x), y(y)
	{
		width = rect.width;
		height = rect.height;
		angle = 0;
		updateRect();
	}
	
	Strand(const sf::Rect<float> &rect)
	{
		x = rand() % (MAX_XY);
		y = rand() % (MAX_XY);
		width = rect.width;
		height = rect.height;
		angle = rand() % NUM_ANGLES;
		updateRect();
	}
	
	void print()
	{
		std::cout << x << " " << y << std::endl;
	}
	
	/** posBias must be odd */
	void mutate(int posBias, int angBias)
	{
		x += rand() % posBias - posBias / 2;
		y += rand() % posBias - posBias / 2;

		if (x<0)
		{
			x = 0;
		}
		if (y<0)
		{
			y = 0;
		}
		
		angle += rand() % angBias - angBias/2;
		angle %= NUM_ANGLES;
		updateRect();
	}
	
	/** byte offset of crossover */
	void crossover(const Strand &other)
	{
		const int strand_size = sizeof(int)*8*2 + 8;
		int offset = rand() % strand_size;
		
#ifdef DEBUG
		// other
		printf("< %x %x %x\n", other.x, other.y, other.angle);
		printf("< %d %d %d\n", other.x, other.y, other.angle);
		// this
		printf("> %x %x %x\n", x, y, angle);
		printf("> %d %d %d\n", x, y, angle);
#endif
		
		unsigned int mask = 1;
		if (offset < sizeof(int)*8)
		{
			for (int i = 0; i < offset; i++)
			{
				mask <<= 1;
			}
			mask--;
			x &= mask;
			mask = ~mask;
			int x_other = other.x & mask;
			x |= x_other;
			
		} else if (offset < 2*sizeof(int)*8)
		{
			x = other.x;
			for (int i = 0; i < offset % (sizeof(int)*8); i++)
			{
				mask <<= 1;
			}
			mask--;
			y &= mask;
			mask = ~mask;
			int y_other = other.y & mask;
			y |= y_other;
			
		} else
		{
			x = other.x;
			y = other.y;
			unsigned char cmask = 1;
			for (int i = 0; i < offset % (sizeof(int)*8); i++)
			{
				cmask <<= 1;
			}
			cmask--;
			angle &= cmask;
			cmask = ~cmask;

			char a_other = other.angle & cmask;
			angle |= a_other;
		}
		if (x<0)
		{
			x = 0;
		}
		if (y<0)
		{
			y = 0;
		}
#ifdef DEBUG
		printf("%x %x %x\n", x, y, angle);
		printf("(%d %d %d)\n", x, y, angle);
#endif
		updateRect();
	}
	
	const Rectangle &getRect() const
	{
		return rect;
	}

	bool operator<(const Strand& rhs) const
	{
		return width*height < rhs.width*rhs.height;
	}
	
	void updateRect()
	{
		float ang_rad = ((float)angle/NUM_ANGLES)*M_PI;
		rect = Rectangle(x, y, width, height, ang_rad);
	}
};

#endif	/* STRAND_H */

