all:
	g++ main.cpp -o bin/Main -g -lsfml-graphics -lsfml-window
debug:
	g++ main.cpp -o bin/Main -DDEBUG -g -lsfml-graphics -lsfml-window
clean:
	rm bin/Main.exe
